---
layout: 2020/post
section: propuestas
category: talks
title: Damegender
---

The gender detection from a name is useful to make gender studies from social networks, mailing lists, software repositories, articles, etc. Nowadays there are various APIs to detect gender from a name. In this speech, we offer a tool to use and compare these apis and a method to predict gender applying machine learning and using a free license and census open data.

## Proposal format

-   [x] &nbsp;Talk (25 minutes)
-   [ ] &nbsp;Lightning talk (10 minutes)

## Description

This talk show you how to install the software, detect gender from names from different sources and acquire mathematical resources to compare methods (accuracies, confusion matrix...)

Who want to explain in detail the machine learning task about gender detection as example of use for scikit and nltk. Very popular libraries for artificial intelligence in Python.

## Target audience

People who likes Python, women who wants gender equality in science.

## Speaker(s)

**David Arroyo Menéndez** is doing the Phd in the URJC. He starts to participate in research groups with e-learning publications at UNED, while he is doing the degree in Computing Science. Later, he was working in the private companies in the web programming area. After of this, he starts master studies in the UCM to understand the methods in the sociology. Later, he decides starts the Phd with Jesús González Barahona by the ethical compromise with the Free Software and the scientific interest measuring the phenomenon with specialization in the gender matters.

### Contact(s)

-   **David Arroyo Menéndez**: d.arroyome at alumnos dot urjc dot es

## Observations

-   <http://www.damegender.net>
-   <https://github.com/davidam/damegender>

## Condiciones

-   [x] &nbsp;I agree to follow the [code of conduct](https://eslib.re/conducta/) and request this acceptance from the attendees and speakers.
-   [x] &nbsp;At least one person among those proposing will be present on the day scheduled for the talk.
