---
layout: 2020/post
section: propuestas
category: talks
title: Doc-As-Code
---

A día de hoy tenemos las herramientas necesarias para conseguir que la documentación de nuestros proyectos sea realmente útil sin que sea un calvario. Hagámoslo.

## Formato de la propuesta

-   [x] &nbsp;Charla (25 minutos)
-   [ ] &nbsp;Charla relámpago (10 minutos)

## Descripción

Git, branches, pair programming, pull request y despliegue continuo no tienen porqué ser conceptos únicamente asociados al código. En esta charla te mostraré cómo documentar tu proyecto a la vez que creas el código y cómo hacerlo participar de todo el flujo hasta llegar a producción, teniéndolo siempre actualizado.

Empezaremos creando una presentación al equipo directivo sobre una idea nueva que hemos tenido, permitiremos al arquitecto explicar el diseño de la solución al equipo mediante diagramas así como a los programadores ir documentando el producto a medida que crece. Todo ello con despliegues en real (si la conexión a Internet lo permite).

## Público objetivo

A todo el mundo, no sólo desarrolladores, con interés por mejorar sus productos.

## Ponente(s)

**Jorge Aguilera**. Desarrollador de software desde hace más de 25 años en múltiples lenguajes y entornos, apasionado del open source y de Asciidoctor. Ponente asiduo en MadridGUG y Codemotion.

### Contacto(s)

-   **Jorge Aguilera**: jorge.aguilera at puravida-software dot com

## Comentarios

Esta charla está basada en la que dí el año pasado en la edición de esLibre 2019 pero ahora sería orientada a un ejemplo práctico.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x] &nbsp;Al menos una persona entre los que la proponen estará presente el día programado para la charla.
