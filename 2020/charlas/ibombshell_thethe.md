# Post-explotación y caza de amenazas

Hoy en día, todos los sistemas Windows disponen de Powershell, por lo que podemos disponer de herramientas como iBombShell para tener un entorno de pentesting en cualquier lugar y en cualquier momento. Una vez se compromete una máquina llega la hora de la post-explotación. Con iBombShell tendremos una shell de pentesting a la que se pueden ir descargando diferentes funcionalidades, en función de cuando se necesiten y sin tocar disco. En el taller se muestran escenarios reales y actuales, de pentesting dónde iBombShell puede trabajar y aportar frescura a los diferentes procesos de un pentesting.

Por otro lado, cada vez es más común encontrar sistemas de threat hunting (caza de amenazas) para detectar amenazas a tiempo y evitar poner en riesgo nuestros sistemas. Uno de los principales inconvenientes a la hora de realizar un threat hunting es la recopilación de información ya que está disponible en un gran número de fuentes, tanto públicas como privadas. Toda esta información suele ser dispersa y a veces incluso volátil. Tal vez en un determinado momento no haya información sobre un IOC en particular (Indicador de compromiso), pero esa situación puede cambiar en unas pocas horas y convertirse en crucial para la investigación. TheTHE es un entorno destinado a ayudar a los analistas y cazadores en las primeras etapas de su trabajo de una manera más fácil, unificada y rápida. En el taller se muestra como montar un entorno de trabajo preparado para la caza de amenazas y el manejo de la herramienta, así como la configuración de los diferentes servicios para llevar a cabo la investigación.

Ambos proyectos son _Open Source_, por lo que puedes animarte a lanzar tus propias lineas de código.

## Descripción

En esta charla se quiere dar una introducción a las herramientas iBombShell y TheTHE, para la post-explotación de sistemas y la caza de amenazas.

## Público objetivo

Estudiantes, desarrolladores, hackers, profesionales o interesados en seguridad informática.

## Ponente(s)

**Álvaro Núñez - Romero Casado**. Trabaja en el laboratorio de la Unidad de Ciberseguridad de Telefónica, ElevenPaths. Ingeniero de Imagen y Sonido. Máster en Seguridad de la Información. Instructor en hackersClub. Entusiasta de la tecnología, la seguridad informática y el mundo maker. Ponente en diferentes congresos como BlackHat Europe 2017 y 2018, RootedCON, CyberCamp, Navaja Negra, HoneyCON, Sh3llCON, EuskalHack, h-c0n, TomatinaCON… Autor de libro _Arduino para Hackers: PoCs & Hacks just for Fun_.

### Contacto(s)

- Nombre: Álvaro Núñez ([@toolsprods](https://twitter.com/toolsprods))
  <alvaro.nunezromero@11paths.com> - _Laboratorio Innovación - ElevenPaths_

## Día

5 de junio.

## Comentarios

- [Repositorio de iBombShell](https://github.com/ElevenPaths/ibombshell)
- [Repositorio de TheTHE](https://github.com/ElevenPaths/thethe)

## Condiciones

- [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/).
- [x] &nbsp;Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
- [x] &nbsp;Acepto coordinarme con la organización de esLibre.
